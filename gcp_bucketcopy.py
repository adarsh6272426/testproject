from google.cloud import storage
import os
from datetime import datetime, timedelta

# Set the path to your service account key file
os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = "/Users/adarsh/Desktop/adarshzone-3bcaf5e283c5.json"

# Define function that creates the bucket
def create_bucket(bucket_name, storage_class='STANDARD', location='europe-west1'): 
    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)
    bucket.storage_class = storage_class

    bucket.create(location=location)
    
    return f"Bucket {bucket.name} successfully created."

# Define function that uploads a file to the bucket
def upload_cs_file(bucket_name, source_file_name, destination_file_name): 
    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)

    blob = bucket.blob(destination_file_name)
    blob.upload_from_filename(source_file_name)

    return True

# Define function that downloads a file from the bucket
def download_cs_file(bucket_name, file_name, destination_file_name): 
    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)

    blob = bucket.blob(file_name)
    blob.download_to_filename(destination_file_name)

    return True

# Define function that lists files in the bucket
def list_cs_files(bucket_name): 
    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)
    files = [blob.name for blob in bucket.list_blobs()]

    return files

# Define function that generates the public URL, default expiration is set to 24 hours
def get_cs_file_url(bucket_name, file_name, expire_in=datetime.today() + timedelta(days=1)): 
    storage_client = storage.Client()

    bucket = storage_client.bucket(bucket_name)
    blob = bucket.blob(file_name)

    url = blob.generate_signed_url(expiration=expire_in)

    return url
